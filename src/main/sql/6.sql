﻿SELECT projects.project_name,projects.cost, ROUND(Avg(developers.salary)) AS Average_Salary
FROM developers JOIN projects_developers ON developers.id = projects_developers.developers
JOIN projects ON projects_developers.projects = projects.id 
GROUP BY projects.project_name, projects.cost ORDER BY Average_Salary ASC 
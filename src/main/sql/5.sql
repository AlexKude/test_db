SELECT companies.COMPANY_NAME, projects.COST, customers.SURNAME AS Customer FROM companies INNER JOIN developers ON companies.id = developers.company
JOIN projects_developers ON developers.id = projects_developers.developers
JOIN projects ON projects_developers.projects = projects.id
JOIN customers_projects ON projects.id = customers_projects.project
JOIN customers ON customers_projects.customer = customers.id 
GROUP BY companies.company_name, projects.COST, customers.Surname ORDER BY projects.COST ASC
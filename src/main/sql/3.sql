SELECT SUM(developers.salary) AS total_java_salary 
FROM developers JOIN developers_skills AS mid ON developers.id = mid.developers
JOIN skills ON mid.skills = skills.id WHERE skills.skill_name = 'Java EE'
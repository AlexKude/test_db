SELECT projects.project_name AS Project, SUM(developers.SALARY) AS Price 
FROM projects JOIN projects_developers AS mid ON projects.id = mid.projects
JOIN developers ON mid.developers = developers.id 
GROUP BY (projects.project_name) ORDER BY Price DESC